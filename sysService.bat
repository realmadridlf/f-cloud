@rem
@rem Licensed to the Apache Software Foundation (ASF) under one or more
@rem contributor license agreements.  See the NOTICE file distributed with
@rem this work for additional information regarding copyright ownership.
@rem The ASF licenses this file to You under the Apache License, Version 2.0
@rem (the "License"); you may not use this file except in compliance with
@rem the License.  You may obtain a copy of the License at
@rem
@rem     http://www.apache.org/licenses/LICENSE-2.0
@rem
@rem Unless required by applicable law or agreed to in writing, software
@rem distributed under the License is distributed on an "AS IS" BASIS,
@rem WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@rem See the License for the specific language governing permissions and
@rem limitations under the License.
@rem

@echo off & setlocal enabledelayedexpansion

cd %~dp0

set LOG_HOME=%~dp0/../logs

set SERVER_NAME=sys

set CLASS_PATH=""

set JAVA_OPTS=-server -Xmx512m -Xms256m -Xmn64m -Xss256k 

set "JAVA_OPTS=%JAVA_OPTS%  

echo Starting the %SERVER_NAME% ...

java %JAVA_OPTS% -Dfile.encoding=UTF-8 -jar service/sys-service/build/libs/sys-service-1.0.0.jar

pause
