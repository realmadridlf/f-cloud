/*
 * Copyright [2022] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.f.client;

import com.f.bo.IdReasonBo;
import com.f.bo.flow.FlowStartProcessBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author liuf
 */
@FeignClient(value = "flow", path = "/flow", contextId = "flow", fallbackFactory = FlowClient.Fallback.class)
public interface FlowClient {

    /**
     * 启动流程
     */
    @PostMapping("/")
    String start(@RequestBody FlowStartProcessBo startProcessBo);

    /**
     * 启动流程
     */
    @DeleteMapping("/")
    void delete(@RequestBody IdReasonBo idReasonBo);

    @Component
    @Slf4j
    class Fallback implements FallbackFactory<FlowClient> {

        @Override
        public FlowClient create(Throwable cause) {
            log.error("异常原因:{}", cause.getMessage(), cause);
            return new FlowClient() {
                @Override
                public String start(FlowStartProcessBo startProcessBo) {
                    log.error("start Fallback:{}", startProcessBo);
                    return null;
                }

                @Override
                public void delete(IdReasonBo idReasonBo) {
                    log.error("delete Fallback:{}", idReasonBo);
                }
            };
        }
    }
}
