/*
 * Copyright [2022] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.f.client;

import com.f.bo.IdStatusBo;
import com.f.bo.StrIdBo;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 流程client
 *
 * @author liuf
 * @date 2023年1月6日
 */
public interface ProcessClient {

    /**
     * 流程任务处理审核通过
     *
     * @param id id
     */
    @PutMapping(value = "/taskCompleted")
    void taskCompleted(@RequestBody StrIdBo id);

    /**
     * 流程任务处理审核通过
     *
     * @param statusBo statusBo
     */
    @PutMapping(value = "/processCompleted")
    void processCompleted(@RequestBody IdStatusBo statusBo);

}
