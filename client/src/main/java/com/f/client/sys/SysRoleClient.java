/*
 * Copyright [2022] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.f.client.sys;

import com.f.bo.sys.SysRoleBo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Set;

/**
 * 系统角色client
 *
 * @author liuf
 * @date 2022/5/11 19:56
 */
@FeignClient(value = "sys", path = "/sysRole", contextId = "sysRoleClient", fallbackFactory = SysRoleClient.Fallback.class)
public interface SysRoleClient {

    /**
     * 获取用户角色
     */
    @GetMapping("/getByUserId")
    Set<Long> getByUserId(@RequestParam(value = "userId") Long userId);

    /**
     * 获取角色
     */
    @GetMapping("/byId")
    SysRoleBo getById(@RequestParam(value = "id") Long id);

    @Component
    @Slf4j
    class Fallback implements FallbackFactory<SysRoleClient> {

        @Override
        public SysRoleClient create(Throwable cause) {
            log.error("异常原因:{}", cause.getMessage(), cause);
            return new SysRoleClient() {
                @Override
                public Set<Long> getByUserId(Long userId) {
                    return null;
                }

                @Override
                public SysRoleBo getById(Long id) {
                    return null;
                }
            };
        }

    }
}
