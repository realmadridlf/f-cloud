/*
 * Copyright [2022] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.f.service;

/**
 * 工作流服务
 *
 * @author liuf
 * @date 2023年1月6日
 */
public interface ProcessService {

    /**
     * 提交流程任务
     *
     * @param id 业务id
     * @return 流程实例id
     */
    String commit(String id);

    /**
     * 撤回流程，只有待审核的可以撤回
     *
     * @param id 业务id
     */
    void unCommit(String id);

    /**
     * 流程任务完成
     *
     * @param id 业务id
     */
    void taskCompleted(String id);

    /**
     * 流程处理完成
     * status = approve 通过
     * status = reject  拒绝
     *
     * @param id     业务id
     * @param status 处理结果
     */
    void processCompleted(String id, String status);
}
