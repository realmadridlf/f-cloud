/*
 * Copyright [2022] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.f.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * StrIdBo bo
 *
 * @author liuf
 * @date 2021/12/15 11:04
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StrIdBo implements Serializable {

    private static final long serialVersionUID = -6424977434004269251L;

    /**
     * id
     */
    @NotNull
    @Length(min = 1, max = 60)
    private String id;
}
