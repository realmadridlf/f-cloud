/*
 * Copyright [2022] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.f.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * IdReasonBo bo
 *
 * @author liuf
 * @date 2023/01/05 11:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IdReasonBo implements Serializable {

    private static final long serialVersionUID = -2644443529075332933L;

    /**
     * id
     */
    @NotNull
    @Length(min = 1, max = 60)
    private String id;

    /**
     * reason 原因
     */
    @NotBlank
    @Length(min = 2, max = 500)
    private String reason;

}
