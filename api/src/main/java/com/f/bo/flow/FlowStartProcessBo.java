/*
 * Copyright [2022] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.f.bo.flow;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Map;

/**
 * 启动流程bo
 *
 * @author liuf
 * @date 2022年12月28日
 */
@Data
@Accessors(chain = true)
public class FlowStartProcessBo implements Serializable {

    private static final long serialVersionUID = -134921282609140059L;

    /**
     * 流程定义key
     */
    @NotBlank
    @Length(min = 3, max = 64)
    private String key;

    /**
     * 业务id
     */
    @NotBlank
    @Length(min = 10, max = 32)
    private String businessKey;

    /**
     * 其他流程变量
     */
    private Map<String, Object> variables;

}
