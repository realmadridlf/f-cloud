/*
 * Copyright [2022] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.f.enums.flow;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * 工作流状态
 *
 * @author liuf
 * @date 2021年12月3日
 */
@RequiredArgsConstructor
@Getter
public enum FlowStatus {

    /**
     * 未提交、待审核、审核中、打回、拒绝、通过、无需审核
     */
    NOT_COMMIT(1),
    COMMIT(2),
    AUDITING(3),
    BACK(4),
    REJECT(5),
    SUCCESS(6),
    NOT_AUDIT(7);

    private final int status;

    public static FlowStatus by(int status) {
        return Arrays.stream(values()).filter(r -> r.status == status).findFirst().orElse(null);
    }
}
