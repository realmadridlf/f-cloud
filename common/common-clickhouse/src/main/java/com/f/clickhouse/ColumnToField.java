package com.f.clickhouse;

import com.google.common.base.CaseFormat;

/**
 * 将数据库列名称映射为实体属性
 *
 * @author liuf
 * @date 2023/6/15 16:30
 */
@FunctionalInterface
public interface ColumnToField {

    ColumnToField DEFAULT = column -> CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, column);

    String toField(String column);
}
