package com.f.clickhouse;

import com.google.common.base.CaseFormat;

/**
 * 将实体属性映射为数据库列名称
 *
 * @author liuf
 * @date 2023/6/15 16:30
 */
@FunctionalInterface
public interface FieldToColumn {

    FieldToColumn DEFAULT = field -> CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, field);

    String toColumn(String field);
}
