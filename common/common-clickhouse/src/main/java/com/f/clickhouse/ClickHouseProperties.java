package com.f.clickhouse;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * clickhouse 配置
 * clickHouse.url=grpc://127.0.0.1:9100/default?load_balancing_policy=random&health_check_interval=5000&failover=2&server_time_zone=Asia/Shanghai
 * clickHouse.username=ck
 * clickHouse.password=feng
 * @author liuf
 * @date 2023/6/15 15:30
 */
@Data
@ConfigurationProperties(prefix = "click-house")
public class ClickHouseProperties {

    private String url;
    private String username;
    private String password;

}
