/*
 * Copyright [2022] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.f.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.f.base.PageRequest;
import com.f.base.Result;
import com.f.bo.FlowNextDto;
import com.f.bo.FlowTaskDto;
import com.f.bo.FlowViewerDto;
import com.f.vo.FlowTaskVo;
import org.flowable.bpmn.model.UserTask;
import org.flowable.task.api.Task;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * 任务service
 *
 * @author liuf
 * @date 2022-12-09
 */
public interface FlowTaskService {

    /**
     * 审批任务
     *
     * @param task 请求实体参数
     */
    Result<Void> complete(FlowTaskVo task);

    /**
     * 驳回任务
     *
     * @param flowTaskVo
     */
    void taskReject(FlowTaskVo flowTaskVo);


    /**
     * 退回任务
     *
     * @param flowTaskVo 请求实体参数
     */
    void taskReturn(FlowTaskVo flowTaskVo);

    /**
     * 获取所有可回退的节点
     *
     * @param flowTaskVo
     * @return
     */
    Result<List<UserTask>> findReturnTaskList(FlowTaskVo flowTaskVo);

    /**
     * 删除任务
     *
     * @param flowTaskVo 请求实体参数
     */
    void deleteTask(FlowTaskVo flowTaskVo);

    /**
     * 认领/签收任务
     *
     * @param flowTaskVo 请求实体参数
     */
    void claim(FlowTaskVo flowTaskVo);

    /**
     * 取消认领/签收任务
     *
     * @param flowTaskVo 请求实体参数
     */
    void unClaim(FlowTaskVo flowTaskVo);

    /**
     * 委派任务
     *
     * @param flowTaskVo 请求实体参数
     */
    void delegateTask(FlowTaskVo flowTaskVo);


    /**
     * 转办任务
     *
     * @param flowTaskVo 请求实体参数
     */
    void assignTask(FlowTaskVo flowTaskVo);

    /**
     * 我发起的流程
     *
     * @param page page对象
     * @return 流程
     */
    Result<Page<FlowTaskDto>> myProcess(PageRequest<FlowTaskDto> page);

    /**
     * 取消申请
     *
     * @param flowTaskVo
     * @return
     */
    Result<Void> stopProcess(FlowTaskVo flowTaskVo);

    /**
     * 撤回流程
     *
     * @param flowTaskVo
     * @return
     */
    Result<Void> revokeProcess(FlowTaskVo flowTaskVo);


    /**
     * 代办任务列表
     *
     * @param pageRequest 分页对象
     * @return 分页数据
     */
    Result<Page<FlowTaskDto>> todoList(PageRequest<FlowTaskDto> pageRequest);


    /**
     * 已办任务列表
     *
     * @param pageRequest 分页对象
     * @return 分页数据
     */
    Result<Page<FlowTaskDto>> finishedList(@RequestBody @Valid PageRequest<FlowTaskDto> pageRequest);

    /**
     * 流程历史流转记录
     *
     * @param procInsId 流程实例Id
     * @return
     */
    Result<Map<String, Object>> flowRecord(String procInsId, String deployId);

    /**
     * 根据任务ID查询挂载的表单信息
     *
     * @param taskId 任务Id
     * @return
     */
    Task getTaskForm(String taskId);

    /**
     * 获取流程过程图
     *
     * @param processId
     * @return
     */
    InputStream diagram(String processId);

    /**
     * 获取流程执行过程
     *
     * @param procInsId
     * @return
     */
    Result<List<FlowViewerDto>> getFlowViewer(String procInsId, String executionId);

    /**
     * 获取流程变量
     *
     * @param taskId
     * @return
     */
    Result<Map<String, Object>> processVariables(String taskId);

    /**
     * 获取下一节点
     *
     * @param flowTaskVo 任务
     * @return
     */
    Result<FlowNextDto> getNextFlowNode(FlowTaskVo flowTaskVo);
}
