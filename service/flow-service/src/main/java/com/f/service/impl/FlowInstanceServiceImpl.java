package com.f.service.impl;


import com.f.base.Result;
import com.f.service.FlowInstanceService;
import com.f.utils.ServiceUtils;
import com.f.vo.FlowTaskVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.flowable.common.engine.api.FlowableObjectNotFoundException;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.task.api.Task;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 流程实例服务实现
 *
 * @author liuf
 * @date 2022-12-09
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class FlowInstanceServiceImpl implements FlowInstanceService {

    private final ProcessEngine processEngine;

    @Override
    public List<Task> queryListByInstanceId(String instanceId) {
        return processEngine.getTaskService().createTaskQuery().processInstanceId(instanceId).active().list();
    }

    @Override
    public void stopProcessInstance(FlowTaskVo vo) {
        String taskId = vo.getTaskId();
    }

    @Override
    public void updateState(Integer state, String instanceId) {

        // 激活
        if (state == 1) {
            processEngine.getRuntimeService().activateProcessInstanceById(instanceId);
        }
        // 挂起
        if (state == 2) {
            processEngine.getRuntimeService().suspendProcessInstanceById(instanceId);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String instanceId, String deleteReason) {

        // 查询历史数据
        HistoricProcessInstance historicProcessInstance = getHistoricProcessInstanceById(instanceId);
        if (historicProcessInstance.getEndTime() != null) {
            processEngine.getHistoryService().deleteHistoricProcessInstance(historicProcessInstance.getId());
            return;
        }
        // 删除流程实例
        processEngine.getRuntimeService().deleteProcessInstance(instanceId, deleteReason);
        // 删除历史流程实例
        processEngine.getHistoryService().deleteHistoricProcessInstance(instanceId);
    }

    @Override
    public HistoricProcessInstance getHistoricProcessInstanceById(String processInstanceId) {
        HistoricProcessInstance historicProcessInstance =
                processEngine.getHistoryService().createHistoricProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
        if (Objects.isNull(historicProcessInstance)) {
            throw new FlowableObjectNotFoundException("流程实例不存在: " + processInstanceId);
        }
        return historicProcessInstance;
    }

    @Override
    public Result<String> startProcessInstanceById(String procDefId, Map<String, Object> variables) {
        variables.put("initiator", String.valueOf(ServiceUtils.getUserId()));
        variables.put("_FLOWABLE_SKIP_EXPRESSION_ENABLED", true);
        processEngine.getRuntimeService().startProcessInstanceById(procDefId, variables);
        return Result.success("流程启动成功");
    }
}