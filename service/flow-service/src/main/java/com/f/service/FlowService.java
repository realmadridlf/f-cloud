/*
 * Copyright [2022] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.f.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.f.base.PageRequest;
import com.f.bo.IdReasonBo;
import com.f.bo.TaskDto;
import com.f.bo.flow.FlowStartProcessBo;

import java.util.Map;

/**
 * <p>
 * 工作流 服务类
 * </p>
 *
 * @author liuf
 * @date 2022年9月23日
 */
public interface FlowService {

    /**
     * 部署
     *
     * @param id 流程设计id
     * @return 部署id
     */
    String deployment(Long id);

    /**
     * 查询流程定义
     *
     * @param deploymentId 部署id
     * @return 流程定义
     */
    String selectDefinition(String deploymentId);

    /**
     * 启动流程实例
     *
     * @param startProcessBo 流程id 业务id 其他业务数据
     * @return 流程实例id
     */
    String startProcess(FlowStartProcessBo startProcessBo);

    /**
     * 删除流程实例
     *
     * @param idReasonBo processInstanceId 流程实例id deleteReason      删除原因
     */
    void deleteProcess(IdReasonBo idReasonBo);

    /**
     * 查询用户任务
     *
     * @param userId 用户id
     * @param page   分页
     * @return 用户任务列表
     */
    Page<TaskDto> queryTask(Long userId, PageRequest<Boolean> page);

    /**
     * 处理流程任务
     *
     * @param status    操作状态 approve:同意 ,reject:拒绝
     * @param taskId    任务id
     * @param variables 任务变量
     */
    void complete(String status, String taskId, Map<String, Object> variables);

}
