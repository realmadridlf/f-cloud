package com.f.bo;

import com.f.vo.SysUserVo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 动态人员、组
 * @author liuf
 * @date 2022-12-09
 */
@Data
public class FlowNextDto implements Serializable {

    private String type;

    private String vars;

    private List<SysUserVo> userList;

//    private List<SysRole> roleList;
}
