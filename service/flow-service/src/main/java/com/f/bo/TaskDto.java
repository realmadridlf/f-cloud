/*
 * Copyright [2022] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.f.bo;

import lombok.Data;
import org.flowable.identitylink.api.IdentityLinkInfo;
import org.flowable.task.api.TaskInfo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
public class TaskDto implements Serializable {

    private String id;

    /**
     * Name or title of the task.
     */
    private String name;

    /**
     * Free text description of the task.
     */
    private String description;

    /**
     * Indication of how important/urgent this task is
     */
    private int priority;

    /**
     * The {@link org.flowable.idm.api.User userId} of the person that is responsible for this task.
     */
    private String owner;

    /**
     * The {@link org.flowable.idm.api.User userId} of the person to which this task is delegated.
     */
    private String assignee;

    /**
     * Reference to the process instance or null if it is not related to a process instance.
     */
    private String processInstanceId;

    /**
     * Reference to the path of execution or null if it is not related to a process instance.
     */
    private String executionId;

    /**
     * Reference to the task definition or null if it is not related to any task definition.
     */
    private String taskDefinitionId;

    /**
     * Reference to the process definition or null if it is not related to a process.
     */
    private String processDefinitionId;

    /**
     * Reference to a scope identifier or null if none is set (e.g. for bpmn process task it is null)
     */
    private String scopeId;

    /**
     * Reference to a sub scope identifier or null if none is set (e.g. for bpmn process task it is null)
     */
    private String subScopeId;

    /**
     * Reference to a scope type or null if none is set (e.g. for bpmn process task it is null)
     */
    private String scopeType;

    /**
     * Reference to a scope definition identifier or null if none is set (e.g. for bpmn process task it is null)
     */
    private String scopeDefinitionId;

    /** The date/time when this task was created */
    private Date createTime;

    /**
     * The id of the activity in the process defining this task or null if this is not related to a process
     */
    private String taskDefinitionKey;

    /**
     * Due date of the task.
     */
    private Date dueDate;

    /**
     * The category of the task. This is an optional field and allows to 'tag' tasks as belonging to a certain category.
     */
    private String category;

    /**
     * The parent task for which this task is a subtask
     */
    private String parentTaskId;

    /**
     * The tenant identifier of this task
     */
    private String tenantId;

    /**
     * The form key for the user task
     */
    private String formKey;

    /**
     * Returns the local task variables if requested in the task query
     */
    private Map<String, Object> taskLocalVariables;

    /**
     * Returns the process variables if requested in the task query
     */
    private Map<String, Object> processVariables;

    /**
     * Returns the identity links.
     */
    private List<? extends IdentityLinkInfo> identityLinks;

    /**
     * The claim time of this task
     */
    private Date claimTime;

    public static TaskDto byTaskInfo(TaskInfo taskInfo){
        TaskDto taskDto = new TaskDto();
        taskDto.setId(taskInfo.getId());
        taskDto.setName(taskInfo.getName());
        taskDto.setDescription(taskInfo.getDescription());
        taskDto.setPriority(taskInfo.getPriority());
        taskDto.setOwner(taskInfo.getOwner());
        taskDto.setAssignee(taskInfo.getAssignee());
        taskDto.setProcessInstanceId(taskInfo.getProcessInstanceId());
        taskDto.setExecutionId(taskInfo.getExecutionId());
        taskDto.setTaskDefinitionId(taskInfo.getTaskDefinitionId());
        taskDto.setProcessDefinitionId(taskInfo.getProcessDefinitionId());
        taskDto.setScopeId(taskInfo.getScopeId());
        taskDto.setSubScopeId(taskInfo.getSubScopeId());
        taskDto.setScopeType(taskInfo.getScopeType());
        taskDto.setScopeDefinitionId(taskInfo.getScopeDefinitionId());
        taskDto.setCreateTime(taskInfo.getCreateTime());
        taskDto.setTaskDefinitionKey(taskInfo.getTaskDefinitionKey());
        taskDto.setDueDate(taskInfo.getDueDate());
        taskDto.setCategory(taskInfo.getCategory());
        taskDto.setParentTaskId(taskInfo.getParentTaskId());
        taskDto.setTenantId(taskInfo.getTenantId());
        taskDto.setFormKey(taskInfo.getFormKey());
        taskDto.setTaskLocalVariables(taskInfo.getTaskLocalVariables());
        taskDto.setProcessVariables(taskInfo.getProcessVariables());
        taskDto.setClaimTime(taskInfo.getClaimTime());
        return taskDto;
    }

}
