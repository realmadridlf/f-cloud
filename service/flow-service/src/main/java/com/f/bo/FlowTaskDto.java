package com.f.bo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 工作流任务
 *
 * @author liuf
 * @date 2022-12-09
 */
@Data
public class FlowTaskDto implements Serializable {

    /**
     * 任务id
     */
    private String taskId;

    /**
     * 执行id
     */
    private String executionId;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 流程定义
     */
    private String taskDefKey;

    /**
     * 流程审核人
     */
    private Long assigneeId;

    /**
     * 流程审核人姓名
     */
    private String assigneeName;

    /**
     * 流程发起人id
     */
    private Long startUserId;

    /**
     * 流程发起人
     */
    private String startUserName;

    /**
     * 分类
     */
    private String category;

    /**
     * 流程变量
     */
    private Object procVars;

    /**
     * 流程本地变量
     */
    private Object taskLocalVars;

    /**
     * 部署id
     */
    private String deployId;

    /**
     * 流程定义id
     */
    private String procDefId;

    /**
     * 流程定义key
     */
    private String procDefKey;

    /**
     * 流程定义名称
     */
    private String procDefName;

    /**
     * 流程版本
     */
    private int procDefVersion;

    /**
     * 流程实例id
     */
    private String procInsId;

    /**
     * 历史流程id
     */
    private String hisProcInsId;

    /**
     * 时常
     */
    private String duration;

    /**
     *
     */
    private FlowCommentDto comment;

    /**
     * 参与人
     */
    private String candidate;
    
    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 完成时间
     */
    private Date finishTime;

}
