/*
 * Copyright [2022] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.f.controller;

import com.f.base.Result;
import com.f.bo.IdReasonBo;
import com.f.bo.flow.FlowStartProcessBo;
import com.f.service.FlowService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 工作流  接口
 *
 * @author liuf
 * @date 2022-09-23
 */
@RestController
@RequestMapping("/flow")
@RequiredArgsConstructor
public class FlowController {

    private final FlowService flowService;

    /**
     * 部署流程
     *
     * @param id 流程设计id
     * @return 结果
     */
    @PostMapping("/deployment/{id}")
    public Result<String> deployment(@PathVariable Long id) {
        return Result.success(flowService.deployment(id));
    }

    /**
     * 部署流程
     *
     * @param id 流程设计id
     * @return 结果
     */
    @GetMapping("/definition/{id}")
    public Result<String> selectDefinition(@PathVariable String id) {
        return Result.success(flowService.selectDefinition(id));
    }

    /**
     * 发起流程
     *
     * @param startProcessBo startProcessBo
     * @return 结果
     */
    @PostMapping("/")
    public String start(@RequestBody @Valid FlowStartProcessBo startProcessBo) {
        return flowService.startProcess(startProcessBo);
    }

    /**
     * 删除流程
     *
     * @param idReasonBo idReasonBo
     */
    @DeleteMapping("/")
    public void delete(@RequestBody @Valid IdReasonBo idReasonBo) {
        flowService.deleteProcess(idReasonBo);
    }


}

