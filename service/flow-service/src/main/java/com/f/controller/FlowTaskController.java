package com.f.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.f.base.PageRequest;
import com.f.base.Result;
import com.f.bo.FlowNextDto;
import com.f.bo.FlowTaskDto;
import com.f.bo.FlowViewerDto;
import com.f.service.FlowTaskService;
import com.f.vo.FlowTaskVo;
import lombok.extern.slf4j.Slf4j;
import org.flowable.bpmn.model.UserTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

/**
 * 工作流任务管理
 *
 * @author liuf
 * @date 2022-12-09
 */
@Slf4j
@RestController
@RequestMapping("/flow/task")
public class FlowTaskController {

    @Autowired
    private FlowTaskService flowTaskService;

    @PostMapping(value = "/myProcess")
    public Result<Page<FlowTaskDto>> myProcess(@RequestBody @Valid PageRequest<FlowTaskDto> page) {
        return flowTaskService.myProcess(page);
    }

    @PostMapping(value = "/stopProcess")
    public Result<Void> stopProcess(@RequestBody FlowTaskVo flowTaskVo) {
        return flowTaskService.stopProcess(flowTaskVo);
    }

    @PostMapping(value = "/revokeProcess")
    public Result<Void> revokeProcess(@RequestBody FlowTaskVo flowTaskVo) {
        return flowTaskService.revokeProcess(flowTaskVo);
    }

    @PostMapping(value = "/todoList")
    public Result<Page<FlowTaskDto>> todoList(@RequestBody @Valid PageRequest<FlowTaskDto> page) {
        return flowTaskService.todoList(page);
    }

    @PostMapping(value = "/finishedList")
    public Result<Page<FlowTaskDto>> finishedList(@RequestBody @Valid PageRequest<FlowTaskDto> page) {
        return flowTaskService.finishedList(page);
    }


    @GetMapping(value = "/flowRecord")
    public Result<Map<String, Object>> flowRecord(String procInsId, String deployId) {
        return flowTaskService.flowRecord(procInsId, deployId);
    }

    @GetMapping(value = "/processVariables/{taskId}")
    public Result<Map<String, Object>> processVariables(@PathVariable(value = "taskId") String taskId) {
        return flowTaskService.processVariables(taskId);
    }

    @PostMapping(value = "/complete")
    public Result<Void> complete(@RequestBody FlowTaskVo flowTaskVo) {
        return flowTaskService.complete(flowTaskVo);
    }

    @PostMapping(value = "/reject")
    public Result<Void> taskReject(@RequestBody FlowTaskVo flowTaskVo) {
        flowTaskService.taskReject(flowTaskVo);
        return Result.success();
    }

    @PostMapping(value = "/return")
    public Result<Void> taskReturn(@RequestBody FlowTaskVo flowTaskVo) {
        flowTaskService.taskReturn(flowTaskVo);
        return Result.success();
    }

    @PostMapping(value = "/returnList")
    public Result<List<UserTask>> findReturnTaskList(@RequestBody FlowTaskVo flowTaskVo) {
        return flowTaskService.findReturnTaskList(flowTaskVo);
    }

    @DeleteMapping(value = "/delete")
    public Result<Void> delete(@RequestBody FlowTaskVo flowTaskVo) {
        flowTaskService.deleteTask(flowTaskVo);
        return Result.success();
    }

    @PostMapping(value = "/claim")
    public Result<Void> claim(@RequestBody FlowTaskVo flowTaskVo) {
        flowTaskService.claim(flowTaskVo);
        return Result.success();
    }

    @PostMapping(value = "/unClaim")
    public Result<Void> unClaim(@RequestBody FlowTaskVo flowTaskVo) {
        flowTaskService.unClaim(flowTaskVo);
        return Result.success();
    }

    @PostMapping(value = "/delegate")
    public Result<Void> delegate(@RequestBody FlowTaskVo flowTaskVo) {
        flowTaskService.delegateTask(flowTaskVo);
        return Result.success();
    }

    @PostMapping(value = "/assign")
    public Result<Void> assign(@RequestBody FlowTaskVo flowTaskVo) {
        flowTaskService.assignTask(flowTaskVo);
        return Result.success();
    }

    @PostMapping(value = "/nextFlowNode")
    public Result<FlowNextDto> getNextFlowNode(@RequestBody FlowTaskVo flowTaskVo) {
        return flowTaskService.getNextFlowNode(flowTaskVo);
    }

    /**
     * 生成流程图
     *
     * @param processId 任务ID
     */
    @GetMapping("/diagram/{processId}")
    public void genProcessDiagram(HttpServletResponse response,
                                  @PathVariable("processId") String processId) throws IOException {
        InputStream inputStream = flowTaskService.diagram(processId);
        try (OutputStream os = response.getOutputStream()) {
            BufferedImage image = ImageIO.read(inputStream);
            response.setContentType("image/png");
            if (image != null) {
                ImageIO.write(image, "png", os);
            }
        }
    }

    /**
     * 生成流程图
     *
     * @param procInsId 流程实例编号
     * @param executionId 任务执行编号
     */
    @GetMapping("/flowViewer/{procInsId}/{executionId}")
    public Result<List<FlowViewerDto>> getFlowViewer(@PathVariable String procInsId,
                                                     @PathVariable String executionId) {
        return flowTaskService.getFlowViewer(procInsId, executionId);
    }
}
