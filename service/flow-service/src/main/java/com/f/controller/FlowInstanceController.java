package com.f.controller;

import com.f.base.Result;
import com.f.service.FlowInstanceService;
import com.f.vo.FlowTaskVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 工作流流程实例管理
 *
 * @author liuf
 * @date 2022-12-09
 */
@Slf4j
@RestController
@RequestMapping("/flow/instance")
@RequiredArgsConstructor
public class FlowInstanceController {

    private final FlowInstanceService flowInstanceService;

    @PostMapping("/startBy/{procDefId}")
    public Result<String> startById( @PathVariable(value = "procDefId") String procDefId,
                                 @RequestBody Map<String, Object> variables) {
        return flowInstanceService.startProcessInstanceById(procDefId, variables);

    }


    @PostMapping(value = "/updateState")
    public Result<Void> updateState(@RequestParam Integer state,
                                   @RequestParam String instanceId) {
        flowInstanceService.updateState(state,instanceId);
        return Result.success();
    }

    @PostMapping(value = "/stopProcessInstance")
    public Result<Void> stopProcessInstance(@RequestBody FlowTaskVo flowTaskVo) {
        flowInstanceService.stopProcessInstance(flowTaskVo);
        return Result.success();
    }

    @DeleteMapping(value = "/delete")
    public Result<Void> delete( @RequestParam String instanceId,
                              @RequestParam(required = false) String deleteReason) {
        flowInstanceService.delete(instanceId,deleteReason);
        return Result.success();
    }
}