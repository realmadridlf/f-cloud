/*
 * Copyright [2022] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.f.controller;

import com.f.base.Result;
import com.f.service.FlowDefinitionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 工作流程定义
 *
 * @author liuf
 * @date 2022-12-09
 */
@Slf4j
@RestController
@RequestMapping("/flow/definition")
public class FlowDefinitionController {

    @Autowired
    private FlowDefinitionService flowDefinitionService;

    @PostMapping("/start/{procDefId}")
    public Result<String> start(@PathVariable(value = "procDefId") String procDefId,
                           @RequestBody Map<String, Object> variables) {
        return flowDefinitionService.startProcessInstanceById(procDefId, variables);
    }

    @PutMapping(value = "/updateState")
    public Result<String> updateState(@RequestParam Integer state,
                              @RequestParam String deployId) {
        flowDefinitionService.updateState(state, deployId);
        return Result.success();
    }

    @DeleteMapping(value = "/{deployIds}")
    public Result<String> delete(@PathVariable String[] deployIds) {
        for (String deployId : deployIds) {
            flowDefinitionService.delete(deployId);
        }
        return Result.success();
    }

}