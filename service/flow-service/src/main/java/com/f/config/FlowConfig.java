package com.f.config;

import com.f.listener.FlowCompletedListener;
import com.f.listener.FlowStartListener;
import com.f.listener.FlowTaskCompletedListener;
import com.f.listener.FlowTaskCreatedListener;
import lombok.RequiredArgsConstructor;
import org.flowable.common.engine.api.delegate.event.FlowableEngineEventType;
import org.flowable.common.engine.api.delegate.event.FlowableEventListener;
import org.flowable.spring.SpringProcessEngineConfiguration;
import org.flowable.spring.boot.EngineConfigurationConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 流程配置类
 */
@RequiredArgsConstructor
@Configuration
public class FlowConfig {

    private final FlowTaskCreatedListener taskCreatedListener;
    private final FlowTaskCompletedListener taskCompletedListener;
    private final FlowCompletedListener completedListener;
    private final FlowStartListener startListener;

    /**
     * 将自定义监听器纳入flowable监听
     */
    @Bean
    public EngineConfigurationConfigurer<SpringProcessEngineConfiguration> globalListenerConfig() {
        return engineConfiguration -> {
            Map<String, List<FlowableEventListener>> map = new HashMap<>(4);
            map.put(FlowableEngineEventType.TASK_CREATED.name(), Collections.singletonList(taskCreatedListener));
            map.put(FlowableEngineEventType.TASK_COMPLETED.name(), Collections.singletonList(taskCompletedListener));
            map.put(FlowableEngineEventType.PROCESS_COMPLETED.name(), Collections.singletonList(completedListener));
            map.put(FlowableEngineEventType.PROCESS_STARTED.name(), Collections.singletonList(startListener));
            engineConfiguration.setTypedEventListeners(map);
        };
    }

}
