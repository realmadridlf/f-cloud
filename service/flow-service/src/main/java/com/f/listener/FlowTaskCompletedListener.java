package com.f.listener;

import com.f.bo.StrIdBo;
import com.f.client.ProcessClient;
import com.f.constant.Constant;
import com.f.utils.ApplicationContextUtils;
import org.flowable.common.engine.api.delegate.event.FlowableEvent;
import org.flowable.common.engine.api.delegate.event.FlowableEventListener;
import org.flowable.common.engine.impl.cfg.TransactionState;
import org.flowable.engine.delegate.event.impl.FlowableEntityWithVariablesEventImpl;
import org.flowable.engine.impl.persistence.entity.ExecutionEntityImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 全局流程执行监听器
 *
 * @author liuf
 * @date 2022年12月29日
 */
@Service
public class FlowTaskCompletedListener implements FlowableEventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(FlowTaskCompletedListener.class);

    @Override
    public void onEvent(FlowableEvent flowableEvent) {
        FlowableEntityWithVariablesEventImpl event = (FlowableEntityWithVariablesEventImpl) flowableEvent;
        LOGGER.info("{},{},{}", event.getProcessInstanceId(), event.getEntity(), event.getVariables());
        Object service = event.getVariables().get(Constant.FLOW_SERVICE);
        if (service != null) {
            ProcessClient client = ApplicationContextUtils.getBean(service.toString(), ProcessClient.class);
            client.taskCompleted(new StrIdBo(((ExecutionEntityImpl) event.getEntity()).getBusinessKey()));
        }
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }

    @Override
    public boolean isFireOnTransactionLifecycleEvent() {
        return false;
    }

    @Override
    public String getOnTransaction() {
        //事务提交后触发
        return TransactionState.COMMITTED.name();
    }
}
