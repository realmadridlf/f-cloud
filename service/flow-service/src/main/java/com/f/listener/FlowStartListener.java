package com.f.listener;

import org.flowable.common.engine.api.delegate.event.FlowableEvent;
import org.flowable.common.engine.api.delegate.event.FlowableEventListener;
import org.flowable.common.engine.impl.cfg.TransactionState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 全局流程执行监听器
 *
 * @author liuf
 * @date 2022年12月29日
 */
@Service
public class FlowStartListener implements FlowableEventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(FlowStartListener.class);

    @Override
    public void onEvent(FlowableEvent flowableEvent) {
        LOGGER.info("FlowStartListener onEvent:{}", flowableEvent);
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }

    @Override
    public boolean isFireOnTransactionLifecycleEvent() {
        return false;
    }

    @Override
    public String getOnTransaction() {
        //事务提交后触发
        return TransactionState.COMMITTED.name();
    }
}
