package com.f.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.f.base.PageRequest;
import com.f.bo.TaskDto;
import com.f.bo.flow.FlowStartProcessBo;
import com.f.constant.Constant;
import com.f.service.FlowService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

@SpringBootTest
class FlowServiceImplTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(FlowServiceImplTest.class);

    @Autowired
    private FlowService flowService;

    @Test
    void deployment() {
        String deploymentId = flowService.deployment(120790366548481L);
        LOGGER.info("deployment:{}", deploymentId);
        Assertions.assertNotNull(deploymentId);
    }

    @Test
    void selectDefinition() {
        String processDefinition = flowService.selectDefinition("8e75f615-6adf-11ed-8e8b-00ff419206fa");
        LOGGER.info("processDefinition:{}", processDefinition);
        Assertions.assertNotNull(processDefinition);
    }

    @Test
    void startProcess() {
        new FlowStartProcessBo()
                .setKey("Leave")
                .setBusinessKey("789456789");
        String leaveApplication = flowService.startProcess(new FlowStartProcessBo()
                .setKey("LeaveApplication1")
                .setBusinessKey("123456789"));
        LOGGER.info("processDefinition:{}", leaveApplication);
        Assertions.assertNotNull(leaveApplication);
    }

    @Test
    void queryTask() {
        Page<TaskDto> tasks = flowService.queryTask(1L, new PageRequest<>(1, 5));
        LOGGER.info("processDefinition:{}", tasks.getRecords());
        Assertions.assertNotNull(tasks);
    }

    @Test
    void complete() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("a", 123);
        map.put("b", "中国766");
        flowService.complete(Constant.FLOW_APPROVE, "075448e6-8d8e-11ed-b930-00ffaabbccdd", map);
    }
}