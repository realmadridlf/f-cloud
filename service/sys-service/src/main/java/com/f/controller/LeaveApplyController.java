/*
 * Copyright [2022] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.f.controller;

import com.f.base.Result;
import com.f.bo.IdStatusBo;
import com.f.bo.StrIdBo;
import com.f.entity.LeaveApply;
import com.f.service.LeaveApplyService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 请假申请  接口
 *
 * @author liuf
 * @date 2022-12-22
 */
@RestController
@RequestMapping("/leaveApply")
@RequiredArgsConstructor
@Getter
public class LeaveApplyController extends BaseController<LeaveApply, LeaveApplyService> {

    private final LeaveApplyService service;

    /**
     * 提交
     *
     * @param id id
     * @return 提交请假
     */
    @PutMapping(value = "/commit")
    public Result<Void> commit(@RequestBody @Valid StrIdBo id) {
        getService().commit(id.getId());
        return Result.success();
    }

    /**
     * 撤回
     *
     * @param id id
     * @return 撤回请假申请
     */
    @PutMapping(value = "/unCommit")
    public Result<Void> unCommit(@RequestBody @Valid StrIdBo id) {
        getService().unCommit(id.getId());
        return Result.success();
    }

    /**
     * 流程任务处理审核通过
     *
     * @param id id
     * @return 无
     */
    @PutMapping(value = "/taskCompleted")
    public void taskCompleted(@RequestBody @Valid StrIdBo id) {
        getService().taskCompleted(id.getId());
    }

    /**
     * 流程任务处理审核通过
     *
     * @param statusBo statusBo
     * @return 无
     */
    @PutMapping(value = "/processCompleted")
    public void processCompleted(@RequestBody @Valid IdStatusBo statusBo) {
        getService().processCompleted(statusBo.getId(), statusBo.getStatus());
    }

}

